# Project Euler #

These are my attempts at solving some interesting and challenging math problems using TDD, written in CoffeeScript.

See `src/` for my progress, and `test/` to see how I got there.

### How do I get set up? ###

* `git clone`
* `npm install`
* `npm test`

### Contribution guidelines ###

* `npm run tdd`
* Test suggestions
* Code review

### Who do I talk to? ###

Find me on [Twitter](https://twitter.com/JaKXz92) or [LinkedIn](https://www.linkedin.com/in/jgkurian).
