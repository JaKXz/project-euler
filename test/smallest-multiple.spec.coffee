smallestMultiple = require "../src/05-smallest-multiple.coffee"

describe "Smallest multiple, evenly divisible by 1..max", ->
  it "should return the correct value for 1..10", ->
    expect(smallestMultiple(10)).to.equal 2520

  it "should return the correct value for 1..20", ->
    expect(smallestMultiple(20)).to.equal 232792560
