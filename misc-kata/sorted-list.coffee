module.exports = (sortBy, list) ->
  sortFn = (left, right) -> left[sortBy] < right[sortBy] ? 1 : -1
  if sortBy.length is 2 and sortBy[0] is "-"
    sortBy = sortBy[1]
    sortFn = (left, right) -> left[sortBy] > right[sortBy] ? 1 : -1
  list.sort sortFn
