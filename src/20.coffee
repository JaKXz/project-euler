math = require 'mathjs'

sumOfDigits = (value) ->
  n = math.bignumber(value)
  value = math.format(math.factorial(n), {notation: 'fixed'})
  result = 0
  digitsArray = String(value).split('').map((digit) -> +digit)
  for digit in digitsArray
    result += digit
  result

console.log(sumOfDigits(100))
