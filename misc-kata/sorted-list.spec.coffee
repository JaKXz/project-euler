sortList = require "../src/sorted-list.coffee"

describe "Sorting a list of objects", ->
  describe "when all the values are the same type", ->
    list = [
      {a: 1, b: 3}
      {a: 3, b: 2}
      {a: 2, b: 40}
      {a: 4, b: 12}
    ]

    it "should sort by the given key, descending", ->
      sortedADesc = [
        {a: 4, b: 12}
        {a: 3, b: 2}
        {a: 2, b: 40}
        {a: 1, b: 3}
      ]
      expect(sortList("a", list)).to.eql sortedADesc

    it "should handle the inverse sort operator", ->
      sortedAsc = [
        {a: 1, b: 3}
        {a: 2, b: 40}
        {a: 3, b: 2}
        {a: 4, b: 12}
      ]
      expect(sortList("-a", list)).to.eql sortedAsc
