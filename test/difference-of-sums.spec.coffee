differenceOfSums = require "../src/06-difference-of-sums.coffee"

describe "Difference of Sums", ->
  it "should return the correct value for 100", ->
    expect(differenceOfSums(100)).to.equal 25164150

  it "should not take longer than 5ms for 1,000,000", ->
    start = Date.now()
    value = differenceOfSums(1000000)
    end = Date.now()
    expect(end - start).to.be.at.most 5
    expect(value).to.be.a("number").and.not.equal Infinity
