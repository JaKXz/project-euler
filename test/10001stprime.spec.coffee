getPrimeAtIndex = require '../src/07-10001st-prime.coffee'

describe "Get nth Prime number", ->
  it "should return the correct value for 6", ->
    expect(getPrimeAtIndex(6)).to.equal 13

  it "should return the correct value for 10,001", ->
    expect(getPrimeAtIndex(10001)).to.equal 104743
