# Project Euler exercise 2
# Sum of Even Fibonacci numbers under 4 000 000
fibonacci = (max) ->
  sequence = [1, 1, 2]
  i = 2
  while sequence[i-1] < max
    sequence[i] = sequence[i-1] + sequence[i-2]
    i++
  sequence

module.exports = (max) ->
  fibonacci(max).reduce ((sum, value) ->
    return sum unless value % 2 is 0
    sum + value
  ), 0
