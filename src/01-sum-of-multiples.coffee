# Project Euler exercise 1
# If we list all the natural numbers below 10 that are
# multiples of 3 or 5, we get 3, 5, 6 and 9.
# The sum of these multiples is 23.

# Find the sum of all the multiples of 3 or 5 below 1000.

module.exports = (max) ->
  x = 3
  y = 5
  counter = 2
  sum = 0
  until x >= max and y >= max
    sum += x if x < max
    sum += y if y < max and y % 3 isnt 0
    x = 3 * counter
    y = 5 * counter
    counter += 1
  sum
