# Project Euler problem 5
# What is the smallest positive number that is evenly
# divisible by all of the numbers from 1 to 20?

module.exports = (max) ->
  result = max ** 3
  x = [max..1]
  until isEvenlyDivisible result, x
    result += max * 4
  result

isEvenlyDivisible = (val, x) -> x.every (n) -> val % n is 0
