# Project Euler exercise 7
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
# we can see that the 6th prime is 13.
# What is the 10 001st prime number?

module.exports = (index) ->
  counter = 0
  index -= 1
  primes = [2]
  value = 3
  while counter < index
    # This ensures `value` is not evenly divisble by the first 65
    # prime numbers, proving prime-ness up to a certain `index`
    if primes[0..65].every ((prime) -> value % prime isnt 0)
      primes.push value
      counter++
    value += 2
  primes[index]
