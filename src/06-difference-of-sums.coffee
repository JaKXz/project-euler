# Project Euler exercise 6
# Find the difference between the sum of the
# squares of the first one hundred natural numbers
# and the square of the sum.

module.exports = (num) ->
  sumOfSquares = 0
  sum = 0
  i = 1
  while i <= num
    sumOfSquares += i*i
    sum += i
    i++
  sum * sum - sumOfSquares
