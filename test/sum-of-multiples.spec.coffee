sumOfMultiples = require '../src/01-sum-of-multiples.coffee'

describe "Sum of Multiples", ->
  it "should return the correct value for 10", ->
    expect(sumOfMultiples(10)).to.equal 23

  it "should return the correct value for 15", ->
    expect(sumOfMultiples(15)).to.equal 45

  it "should return the correct value for 50", ->
    expect(sumOfMultiples(50)).to.equal 543

  it "should return the correct value for 1000", ->
    expect(sumOfMultiples(1000)).to.equal 233168

  it "should not take more than 5ms for 1,000,000", ->
    start = Date.now()
    value = sumOfMultiples(1000000)
    end = Date.now()
    expect(end - start).to.be.at.most 5
    expect(value).to.be.a("number").and.not.equal Infinity

  it "should not timeout for 1 billion", (done) ->
    value = sumOfMultiples(1000000000)
    done()
