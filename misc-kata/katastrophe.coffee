INITIAL_STRENGTH_VALUE = 1000
RATE_OF_DECAY = 1/100

earthquakeStrength = (earthquake) ->
  earthquake.reduce ((total, shockwave) ->
    total * shockwave.reduce ((sum, tremor) ->
      sum + tremor
    ), 0
  ), 1

exponentialDecay = (age) ->
  INITIAL_STRENGTH_VALUE * Math.E ** (-RATE_OF_DECAY * age)

module.exports = strongEnough = (earthquake, age) ->
  if earthquakeStrength(earthquake) < exponentialDecay(age)
    "Safe!"
  else
    "Needs Reinforcement!"
