sumOfEvenFibonacci = require '../src/02-sum-of-even-fibonacci.coffee'

describe "Sum of Even Fibonacci numbers", ->
  it "should return the correct value for 4,000,000", ->
    expect(sumOfEvenFibonacci(4000000)).to.equal 4613732

  it "should not take more than 1ms for the maximum allowable value", ->
    start = Date.now()
    value = sumOfEvenFibonacci(Number.MAX_VALUE/4)
    end = Date.now()
    expect(end - start).to.be.at.most 1
    expect(value).to.be.a("number").and.not.equal Infinity
