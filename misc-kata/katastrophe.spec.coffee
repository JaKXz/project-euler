strongEnough = require "../src/katastrophe.coffee"

describe "Katastrophe", ->
  it "should know when the city is safe", ->
    expect(strongEnough([[2,3,1],[3,1,1],[1,1,2]], 2)).to.equal "Safe!"
    expect(strongEnough([[5,8,7],[3,3,1],[4,1,2]], 2)).to.equal "Safe!"

  it "should warn when the city is not safe", ->
    expect(strongEnough([[5,8,7],[3,3,1],[4,1,2]], 3))
      .to.equal "Needs Reinforcement!"
